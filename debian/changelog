partimage-doc (20050720-5) unstable; urgency=medium

  * QA upload.
  * Refresh packaging.
    + Use "3.0 (quilt)" source format.
    + Bump debhelper compat to v13. (Closes: #965768)
    + Bump Standards-Version to 4.5.1.
    + Use dh sequencer.
    + Mark binary package as Multi-Arch: foreign.
  * debian/patches/0001: Update install path.
  * debian/doc-base: Fix lintian warnings.

 -- Boyuan Yang <byang@debian.org>  Thu, 22 Apr 2021 11:12:13 -0400

partimage-doc (20050720-4.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 16:29:24 +0100

partimage-doc (20050720-4) unstable; urgency=medium

  * Orphan the package.

 -- Michael Biebl <biebl@debian.org>  Sat, 22 Mar 2014 23:34:23 +0100

partimage-doc (20050720-3) unstable; urgency=low

  * debian/rules
    - Don't ignore make clean errors.
    - Build the package in the binary-indep target.
  * debian/doc-base
    - Remove unnecessary white space.
  * debian/control
    - Use new Homepage field to specify the upstream URL.
    - Bump Standards-Version to 3.7.3.
    - Fix a few spelling errors in the long description.
  * debian/docs
    - No longer install fdl-license.html, a copy of the GFDL.
  * debian/copyright
    - Add a pointer to /usr/share/common-licenses/GFDL.

 -- Michael Biebl <biebl@debian.org>  Mon, 04 Feb 2008 01:23:49 +0100

partimage-doc (20050720-2) unstable; urgency=low

  * Bump Standards-Version to 3.7.2, no further changes required.
  * Bump debhelper compatibility to 5. Tightened build dependency accordingly.
  * Added homepage URL to package description.
  * Update maintainer email address to biebl@debian.org.

 -- Michael Biebl <biebl@debian.org>  Fri, 20 Oct 2006 01:10:32 +0200

partimage-doc (20050720-1) unstable; urgency=low

  * New maintainer.
  * Updated to standards version 3.6.2.
  * Removed build dependency to autoconf and automake.
  * Removed postinst and prerm as they are created by dh_installdocs
    automatically. Closes: #292983
  * Updated documentation from CVS.

 -- Michael Biebl <biebl@teco.edu>  Wed, 20 Jul 2005 16:44:32 +0200

partimage-doc (20020126-8) unstable; urgency=low

  * Added new doc-base description. Closes: #217409

 -- Sergio Rua <srua@debian.org>  Fri, 24 Oct 2003 15:16:20 +0100

partimage-doc (20020126-7) unstable; urgency=low

  * Moved man pages the partimage and partimage-server packages.

 -- Sergio Rua <srua@debian.org>  Wed,  9 Jul 2003 13:39:10 +0100

partimage-doc (20020126-6) unstable; urgency=low

  * Corrected path in doc-base. Closes: #189748

 -- Sergio Rua <srua@debian.org>  Sun, 27 Apr 2003 10:04:38 +0100

partimage-doc (20020126-5) unstable; urgency=low

  * Rebuilding the package with "linuxdoc-tools" instead of sgmltools-lite
  fix the previous problem with the index.html. Closes: #186363
  * Fixed typo error in postinst and prerm. Closes: #183048

 -- Sergio Rua <srua@debian.org>  Thu, 27 Mar 2003 21:11:50 +0000

partimage-doc (20020126-4) unstable; urgency=low

  * Created symbolic link from index-1.html to index.html. Closes: #186363

 -- Sergio Rua <srua@debian.org>  Wed, 26 Mar 2003 22:41:49 +0000

partimage-doc (20020126-3) unstable; urgency=low

  * Fixed links in the doc directory. Close: #183048
  * Changed sgml tool from linuxdoc to sgmltools-lite

 -- Sergio Rua <srua@debian.org>  Sun,  2 Mar 2003 11:00:34 +0000

partimage-doc (20020126-2) unstable; urgency=low

  * Added doc-base file.

 -- Sergio Rua <srua@debian.org>  Wed,  2 Oct 2002 20:05:40 +0200

partimage-doc (20020126-1) unstable; urgency=low

  * Initial Release.

 -- Sergio Rua <srua@debian.org>  Thu,  8 Mar 2002 12:16:36 +0100
